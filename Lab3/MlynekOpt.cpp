#include "MlynekOpt.h"
#include "TUpdater.h"
#include "CritFinder.h"

int MlynekOpt::sort(std::vector<Task>& taskVector)
{
	int tempC = 0;
	bool reset = true;
	int bestCrit = 0;
	std::vector<Task> tempTaskVector = taskVector;

	while (reset)
	{
		TUpdater::update(taskVector);
		bestCrit = CritFinder::find(taskVector);
		for (int i = 0; i < taskVector.size() - 1; i++)
		{
			for (int k = i + 1; k < taskVector.size(); k++)
			{
				reset = false;
				std::swap(tempTaskVector[i], tempTaskVector[k]);
				TUpdater::update(tempTaskVector);
				tempC = CritFinder::find(tempTaskVector);

				if (tempC < bestCrit)
				{
					taskVector = tempTaskVector;
					bestCrit = tempC;
					reset = true;
					break;
				}
				else
				{
					tempTaskVector = taskVector;
				}

			}
			if (reset)
				break;
		}
	}
	return bestCrit;
}
