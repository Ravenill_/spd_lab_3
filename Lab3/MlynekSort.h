#pragma once
#include <vector>
#include "Task.h"

class MlynekSort
{
private:
    static int counter;

public:    
    static void sort(std::vector<Task>& taskVector);
};