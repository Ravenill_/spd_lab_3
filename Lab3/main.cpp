#include "Reader.h"
#include <cstdlib>
#include "CritFinder.h"
#include "TUpdater.h"
#include "SortDed.h"
#include "MlynekSort.h"
#include "MlynekOpt.h"

void printCmax()
{
	std::cout << "\n";
	std::cout << "Value of Cmax: ";
}

int main()
{
	std::vector<Task> taskVect; //wektor, kt�ry przechowuje taski

	Reader reader; //obiekt czytaj�cy z pliku. W konstruktorze mo�esz poda� jego nazw�.
	reader.readFromFileTo(taskVect); //fatkycznie wczytanie z pliku do wektora
    std::cout << "\n\nOriginal data\n";
    for (auto& task : taskVect)
        std::cout << task;

    TUpdater::update(taskVect);
    std::cout << "\n\nAfter TUpdate\n";
    for (auto& task : taskVect)
        std::cout << task;
    std::cout << "Critical Value: " << CritFinder::find(taskVect) << "\n";
    /*
    SortDed::sort(taskVect);
    TUpdater::update(taskVect);
    std::cout << "\n\nSort by Ded and TUpdate\n";
    for (auto& task : taskVect)
        std::cout << task;
    std::cout << "Critical Value: " << CritFinder::find(taskVect) << "\n";
    */
    /*
    MlynekSort::sort(taskVect); 
    TUpdater::update(taskVect);
    std::cout << "\n\nMlynekSort and TUpdate\n";
    for (auto& task : taskVect)
        std::cout << task;
    std::cout << "Critical Value: " << CritFinder::find(taskVect) << "\n";
    */
    ///*
    MlynekOpt::sort(taskVect);
    TUpdater::update(taskVect);
    std::cout << "\n\nMlynekSort and TUpdate\n";
    for (auto& task : taskVect)
        std::cout << task;
    std::cout << "Critical Value: " << CritFinder::find(taskVect) << "\n";
    //*/
	system("PAUSE");
}
