#pragma once
#include <vector>
#include "Task.h"

class CritFinder
{
public:
    static int find(const std::vector<Task>& taskVector);
};