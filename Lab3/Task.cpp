#include "Task.h"

Task::Task()
: p(0)
, w(0)
, d(0)
, t(0)
{

}

Task& Task::operator=(const Task& arg)
{
	p = arg.p;
	w = arg.w;
	d = arg.d;
    t = arg.t;
	
	return *this;
}

std::istream& operator >> (std::istream& inputStream, Task& obj)
{
	inputStream >> obj.p;
	inputStream >> obj.w;
	inputStream >> obj.d;

	return inputStream;
}

std::ostream& operator << (std::ostream & inputStream, Task & obj)
{
	std::cout << "p:" << obj.p << " w:" << obj.w << " d:" << obj.d << " t:" << obj.t << "\n";
	return inputStream;
}
