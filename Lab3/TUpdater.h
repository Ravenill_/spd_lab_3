#pragma once
#include <vector>
#include "Task.h"

class TUpdater
{
public:
   static void update(std::vector<Task>& taskVector);
};