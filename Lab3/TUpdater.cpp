#include "TUpdater.h"
#include <algorithm>

void TUpdater::update(std::vector<Task>& taskVector)
{
    int C = 0;

    for (auto& task : taskVector)
    {
        C += task.p;
        task.t = std::max(0, C - task.d);
    }
}
