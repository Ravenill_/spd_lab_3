#include "CritFinder.h"

int CritFinder::find(const std::vector<Task>& taskVector)
{
    int result = 0;

    for (auto& task : taskVector)
    {
        result += (task.t * task.w);
    }

    return result;
}
