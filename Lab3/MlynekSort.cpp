#include "MlynekSort.h"
#include <algorithm>
#include "TUpdater.h"
#include "CritFinder.h"
#include "SortDed.h"

int MlynekSort::counter = 0;

void MlynekSort::sort(std::vector<Task>& taskVector)
{
    SortDed::sort(taskVector);
    TUpdater::update(taskVector);
    int crit_value_before = CritFinder::find(taskVector);
    int crit_value_after = crit_value_before;

    int elem = 0;
    int max_wt = 0;
    for (int i = 0; i < taskVector.size() - 1; i++)
    {
        int act_wt = (taskVector[i].t * taskVector[i].w);
        if (act_wt > max_wt)
        {
            max_wt = act_wt;
            elem = i;
        }
    }

    do
    {
        if (crit_value_after < crit_value_before)
        {
            crit_value_before = crit_value_after;
        }
        if ((elem - 1) < 0)
        {
            break;
        }

        std::swap(taskVector[elem - 1], taskVector[elem]);
        TUpdater::update(taskVector);
        crit_value_after = CritFinder::find(taskVector);
        elem--;
    } while (crit_value_after < crit_value_before);

    std::swap(taskVector[elem], taskVector[elem + 1]);
    counter++;

    if (counter < taskVector.size())
    {
        sort(taskVector);
    }

    /*
    std::sort(taskVector.begin(), taskVector.end(), [](const Task& a, const Task& b)->bool { return (a.w > b.w); });
    std::sort(taskVector.begin(), taskVector.end(), [](const Task& a, const Task& b)->bool { if (a.w == b.w) { return (a.d < b.d); } return false; });
    TUpdater::update(taskVector);

    /*
    for (int i = 0; i < taskVector.size() - 1; i++)
    {
        if (taskVector[i].d == taskVector[i + 1].d)
        {

        }
    }
    

    int crit_value_after = CritFinder::find(taskVector);

    if (crit_value_after < crit_value_before)
    {
        sort(taskVector);
    */
}
