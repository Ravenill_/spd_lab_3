#pragma once
#include <iostream>

class Task
{
public:
	int p;
	int w;
	int d;

    int t;

	Task();
	Task(int _p, int _w, int _d) : p(_p), w(_w), d(_d), t(0) {}

	friend std::istream& operator >> (std::istream& inputStream, Task& obj);
	friend std::ostream& operator << (std::ostream& outputStream, Task& obj);
	Task& operator =(const Task& arg);
};