#include "SortDed.h"
#include <algorithm>

void SortDed::sort(std::vector<Task>& taskVector)
{
    std::sort(taskVector.begin(), taskVector.end(), [](const Task& a, const Task& b)->bool { return a.d < b.d; });
}
